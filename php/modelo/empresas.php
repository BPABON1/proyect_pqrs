<?php
require_once("conexion.php");
/*
*   fecha: 04-mayo-2021
*   autor: branm aldair pabon
*   proposito: adinistrar las tablas de las EMPRESAS
*/
class EMPRESAS extends Conexion
{
    private $id;
    private $nombre;
    private $estado_empresa;
 
        /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param mixed $nombre
     *
     * @return self
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;

        return $this;
    }
        /**
     * @return mixed
     */
    public function getEstadoEmpresa()
    {
        return $this->estado_empresa;
    }

    /**
     * @param mixed $estado_empresa
     *
     * @return self
     */
    public function setEstadoEmpresa($estado_empresa)
    {
        $this->estado_empresa = $estado_empresa;

        return $this;
    }
    
    public function __construct() {
        parent::__construct();

        $this->table = "empresa";
    }
    /*
    * PROPOSITO: TRAER TODA LA INFORMACION DETALLADA DE LA PQRS CREADA POR EL USUARIO LOGUEADO PARA VER EL ESTADO
    */
    public function GetAll()
    {
        try {
            $query = "SELECT * FROM $this->table WHERE estado_empresa = 'ACTIVO'";
            $result = parent::consult($query);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * proposito: agregar una nueva empresa
    */
    public function InsertEmpresa()
    {
        try {
            $sql = "INSERT INTO $this->table 
                        (
                            nombre
                        )
                    VALUES 
                        (
                        '$this->nombre'
                        )";
            $result = parent::save($sql);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * proposito: editar los datos de la empresa 
    */
    public function UpdateEmpresa()
    {
        try {
            $sql = "UPDATE $this->table 
                        SET nombre = '$this->nombre',estado_empresa = '$this->estado_empresa'
                            WHERE id = $this->id";
            $result = parent::save($sql);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * proposito: Eliminar la empresa 
    */
    public function DeleteEmpresa()
    {
        try {
            $sql = "DELETE FROM $this->table WHERE id = $this->id";
            $result = parent::save($sql);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * PROPOSITO: TRAER TODA LA INFORMACION DETALLADA DE LA PQRS CREADA POR EL USUARIO LOGUEADO PARA VER EL ESTADO
    */
    public function GetAllTotal()
    {
        try {
            $query = "SELECT * FROM $this->table";
            $result = parent::consult($query);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
}
?>