<?php
require_once("conexion.php");
/*
*   fecha: 04-mayo-2021
*   autor: branm aldair pabon
*   proposito: adinistrar las tablas de las de gerente y empresas donde 
*/
class GERENTEEMPRESAS extends Conexion
{
    private $id;
    private $id_usuario;
    private $id_empresa;
     /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdUsuario()
    {
        return $this->id_usuario;
    }

    /**
     * @param mixed $id_usuario
     *
     * @return self
     */
    public function setIdUsuario($id_usuario)
    {
        $this->id_usuario = $id_usuario;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdEmpresa()
    {
        return $this->id_empresa;
    }

    /**
     * @param mixed $id_empresa
     *
     * @return self
     */
    public function setIdEmpresa($id_empresa)
    {
        $this->id_empresa = $id_empresa;

        return $this;
    }

    
    public function __construct() {
        parent::__construct();

        $this->table = "gerente_empresa";
    }
    /*
    * proposito: validar que la empresa no este sociada a algun gerente 
    */
    public function ValidaEmpresa()
    {
        try {
            $query = "SELECT * FROM $this->table WHERE id_empresa = $this->id_empresa";
            $result = parent::consult($query);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * proposito: agregar una relacion entre gerente y empresa
    */
    public function InsertRelacion()
    {
        try {
            $sql = "INSERT INTO $this->table 
                        (
                            id_usuario,
                            id_empresa
                        )
                    VALUES 
                        (
                        $this->id_usuario,
                        $this->id_empresa
                        )";
            $result = parent::save($sql);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
}
?>