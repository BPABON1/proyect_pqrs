<?php
require_once("conexion.php");
class User extends Conexion
{
    private $id;
    private $nombres;
    private $apellidos;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getnombres()
    {
        return $this->nombres;
    }

    /**
     * @param mixed $nombres
     *
     * @return self
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * @param mixed $apellidos
     *
     * @return self
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }
    public function __construct() {
        parent::__construct();

        $this->table = "usuarios";
    }
    /*
    * PROPOSITO: LISTAR TODOS LOS VALORES DE ESA CONSULTA
    */
    public function Getall()
    {
        try {
            $query = "SELECT * FROM $this->table";
            $result = parent::consult($query);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * proposito: agregar un nuevo registro
    */
    public function Insert()
    {
        try {
            $sql = "INSERT INTO $this->table 
                        (
                            nombres,
                            apellidos
                        )
                    VALUES 
                        (
                        '$this->nombres',
                        '$this->apellidos')";
            $result = parent::save($sql);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
}
?>