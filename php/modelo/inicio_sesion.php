<?php
		/*
		*	fecha: 03-mayo-2021
		*	proposito: validar en la tabla de usuarios si la contraseña y el usuario son los correctos
		*/
    require_once('usuarios.php');
    $usuario = $_POST['usuario'];
    $contraseña = $_POST['password'];

    $obj_consulta = new Usuarios();
    if(!empty($usuario)){
        $obj_consulta->setUser($usuario);
    }
    if(!empty($contraseña)){
        $obj_consulta->setPassword(md5($contraseña));
    }
    $detalle     = $obj_consulta->Getall();

    $max  = mysqli_num_rows($detalle);
    if ($max > 0) {
        $row = mysqli_fetch_object($detalle);
        session_start();

        $_SESSION['id_usuario'] = $row->id;
        $_SESSION['nombres'] = $row->nombres;
        $_SESSION['user'] = $row->user;
        $_SESSION['rol'] = $row->rol;
        header('Location: ../../pages');
    } else {
        echo "<script type='text/javascript'>
        alert('Datos de usuario incorrectos');
        window.location.href='../../index.html';
        </script>";
    }
?>