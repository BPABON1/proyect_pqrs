<?php
/*
* archivo para destruir la variable de sesion y evitar el ingreso por la URL 
* si se quiciera realizar una auditoria para el cierre de sesion seria en este archivc
*/
    session_start();
    $sesion_finalizada = 'NO';
    if($_SESSION['id_usuario']){
        session_destroy();
        $sesion_finalizada = 'SI';
    }
    if($sesion_finalizada == 'SI'){
        header('Location: ../../');
    }
?>