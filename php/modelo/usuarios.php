<?php
require_once("conexion.php");
class Usuarios extends Conexion
{
    private $id;
    private $nombres;
    private $apellidos;
    private $user;
    private $password;
    private $tipo;
    private $rol;
    private $email;
    private $documento;
    private $usuario_crea;
    private $estado;
        /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombres()
    {
        return $this->nombres;
    }

    /**
     * @param mixed $nombres
     *
     * @return self
     */
    public function setNombres($nombres)
    {
        $this->nombres = $nombres;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getApellidos()
    {
        return $this->apellidos;
    }

    /**
     * @param mixed $apellidos
     *
     * @return self
     */
    public function setApellidos($apellidos)
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     *
     * @return self
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     *
     * @return self
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     *
     * @return self
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRol()
    {
        return $this->rol;
    }

    /**
     * @param mixed $rol
     *
     * @return self
     */
    public function setRol($rol)
    {
        $this->rol = $rol;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDocumento()
    {
        return $this->documento;
    }

    /**
     * @param mixed $documento
     *
     * @return self
     */
    public function setDocumento($documento)
    {
        $this->documento = $documento;

        return $this;
    }
        /**
     * @return mixed
     */
    public function getUsuarioCrea()
    {
        return $this->usuario_crea;
    }

    /**
     * @param mixed $usuario_crea
     *
     * @return self
     */
    public function setUsuarioCrea($usuario_crea)
    {
        $this->usuario_crea = $usuario_crea;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }
    
    public function __construct() {
        parent::__construct();

        $this->table = "usuario";
    }
    /*
    * PROPOSITO: LISTAR TODOS LOS VALORES DE ESA CONSULTA
    */
    public function Getall()
    {
        try {
            $query = "SELECT * FROM $this->table WHERE user ='$this->user' AND password = '$this->password' AND estado ='ACTIVO'";
            $result = parent::consult($query);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * PROPOSITO: TRAER TODA LA INFORMACION DETALLADA DEL USUARIO QUE ESTA LOGUEADO
    */
    public function GetInfoUser()
    {
        try {
            $query = "SELECT * FROM $this->table WHERE id =$this->id";
            $result = parent::consult($query);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * proposito: validar por nombre de usuario o por numero de documento que ese usuario no exista
    */
    public function ValidaUser()
    {
        try {
            $query = "SELECT * FROM $this->table WHERE user ='$this->user' OR documento = $this->documento";
            $result = parent::consult($query);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * proposito: agregar un nuevo cliente a la base de datos
    */
    public function InsertCliente()
    {
        try {
            $sql = "INSERT INTO $this->table 
                        (
                            nombres,
                            apellidos,
                            user,
                            password,
                            tipo,
                            email,
                            documento,
                            rol,
                            estado
                        )
                    VALUES 
                        (
                        '$this->nombres',
                        '$this->apellidos',
                        '$this->user',
                        '$this->password',
                        '$this->tipo',
                        '$this->email',
                        $this->documento,
                        1,
                        'ACTIVO'
                        )";
            $result = parent::save($sql);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * proposito: agregar un nuevo operario a la base de datos relacionado al gerente por la columna usuario crea
    */
    public function InsertOperarios()
    {
        try {
            $sql = "INSERT INTO $this->table 
                        (
                            nombres,
                            apellidos,
                            user,
                            password,
                            tipo,
                            email,
                            documento,
                            rol,
                            usuario_crea,
                            estado
                        )
                    VALUES 
                        (
                        '$this->nombres',
                        '$this->apellidos',
                        '$this->user',
                        '$this->password',
                        '$this->tipo',
                        '$this->email',
                        $this->documento,
                        $this->rol,
                        $this->usuario_crea,
                        'ACTIVO'
                        )";
            $result = parent::save($sql);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * PROPOSITO: TRAER TODA LA INFORMACION DETALLADA DE LOS USUARIOS CREADOS POR EL GERENTE
    */
    public function GetInfoOper()
    {
        try {
            $query = "SELECT * FROM $this->table WHERE usuario_crea =$this->usuario_crea";
            $result = parent::consult($query);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * proposito: actualizar la informacion basica del usuario tipo operario
    */
    public function UpdateOperarios()
    {
        try {
            $sql = "UPDATE $this->table 
                        SET nombres = '$this->nombres',apellidos = '$this->apellidos',email='$this->email',
                            estado = '$this->estado'
                            WHERE id = $this->id";
            $result = parent::save($sql);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * proposito: agregar un nuevo gerente y retornar el id para asociar a la empresa
    */
    public function InsertGerente()
    {
        try {
            $sql = "INSERT INTO $this->table 
                        (
                            nombres,
                            apellidos,
                            user,
                            password,
                            tipo,
                            email,
                            documento,
                            rol,
                            estado
                        )
                    VALUES 
                        (
                        '$this->nombres',
                        '$this->apellidos',
                        '$this->user',
                        '$this->password',
                        '$this->tipo',
                        '$this->email',
                        $this->documento,
                        $this->rol,
                        'ACTIVO'
                        )";
            $result = parent::save($sql);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * PROPOSITO: TRAER TODA LA INFORMACION DETALLADA DE LOS USUARIOS CREADOS POR EL TIPO GERENTE
    */
    public function GetInfoGerente()
    {
        try {
            $query = "SELECT u.id AS id_usuari,
                             u.apellidos AS apellidos_user,
                             u.nombres AS nombres_user,
                             u.user AS user_u,
                             u.tipo AS user_tipo,
                             u.rol AS user_rol,
                             u.email AS user_email,
                             u.estado AS user_estado,
                             e.id AS id_empresa,
                             e.nombre AS id_nombre_empresa,
                             g.*
                        FROM usuario u
                            INNER JOIN gerente_empresa g ON g.id_usuario = u.id
                            INNER JOIN empresa e ON g.id_empresa = e.id 
                        WHERE u.rol = 2";
            $result = parent::consult($query);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
}
?>