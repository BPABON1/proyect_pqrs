<?php
		/*
		*	fecha: 03-mayo-2021
		*	proposito: Validar que el usuario que se esta registrando no exista en la BD ni por el nombre de usuario
		* 	ni por el numero de documento
        */
        $nombre_user = $_POST['nombre_user'];
        $apellido_user = $_POST['apellido_user'];
        $documento_user = $_POST['documento_user'];
        $email_user = $_POST['email_user'];
        $name_user = $_POST['name_user'];
        $password = $_POST['password'];
        $veri_password = $_POST['veri_password'];
    try {
        require_once('usuarios.php');
        // validar que las contraseñas fueron ingresadas y que son iguales
        if(isset($password) && isset($veri_password)){
            if($password == $veri_password){
                $seguir = 'SI';
            }
            else{
                $seguir = 'NO';
            }
        }
        else{
            $seguir = 'NO';
        }
        // validar que todos los campos fueron ingresados 
        if(!empty($nombre_user) && !empty($apellido_user) && !empty($documento_user) && !empty($email_user) && !empty($name_user)){
            $faltan_campos = 'NO';
        }
        else{
            $faltan_campos = 'SI'; 
        }
        if($faltan_campos == 'NO'){
            if($seguir == 'SI'){
                $obj_consulta = new Usuarios();
                $obj_consulta->setNombres('');
                if(isset($nombre_user)){
                    $obj_consulta->setNombres(str_replace("'",'',$nombre_user));
                }
                $obj_consulta->setApellidos('');
                if(isset($apellido_user)){
                    $obj_consulta->setApellidos(str_replace("'",'',$apellido_user));
                }
                $obj_consulta->setUser('');
                if(isset($name_user)){
                    $obj_consulta->setUser(str_replace("'",'',$name_user));
                }
                if(isset($password)){
                    $obj_consulta->setPassword(str_replace("'",'',md5($password)));
                }
                $obj_consulta->setTipo('Cliente');
                $obj_consulta->setEmail('');
                if(isset($email_user)){
                    $obj_consulta->setEmail(str_replace("'",'',$email_user));
                }
                $obj_consulta->setDocumento('');
                if(isset($documento_user)){
                    $obj_consulta->setDocumento($documento_user);
                }
                // consultar que el nombre de usuario o numero de documento no este registrado en el sistema 
                $validacion     = $obj_consulta->ValidaUser();
                $max  = mysqli_num_rows($validacion);
                $contador = 0;
                if ($max > 0) {
                    echo "<script type='text/javascript'>
                        alert('Los datos de este usuario ya se encuentran registrados anteriormente por favor valide los datos que intenta guardar.');
                        window.location.href='../../index.html';
                        </script>";
                } else {
                    $query = $obj_consulta->InsertCliente();
                    if($query){
                        echo "<script type='text/javascript'>
                        alert('Usuario registrado de forma correcta');
                        window.location.href='../../index.html';
                        </script>";
                    }
                    else{
                        echo "<script type='text/javascript'>
                        alert('El usuario no pudo ser registrados por favor valide la información');
                        window.location.href='../../index.html';
                        </script>";
                    }
                }
            }
            else{
                echo "<script type='text/javascript'>
                alert('Las contraseñas no coinciden verifique la información');
                window.location.href='../../index.html';
                </script>"; 
            }
        }
        else{
            echo "<script type='text/javascript'>
                alert('Verifica que todos los campos fueron ingresados de forma correcta ');
                window.location.href='../../index.html';
                </script>";
        }
    } catch (Exception $e) {
        echo "<script type='text/javascript'>
                alert('Error por favor comuníquese con el administrador del sistema');
                window.location.href='../../index.html';
                </script>";
    }
?>