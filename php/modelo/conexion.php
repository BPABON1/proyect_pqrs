﻿<?php

class Conexion{


    private $conexion;
    private $ruta;
    private $total_consultas;

    private $host = "localhost";
    private $user = "root";
    private $pass = "";
    private $db   = "pqrs";

    private $resultado;

/*
*   proposito: realizar la conexion a la base de datos de prueba
*/
    public function __construct(){
       // $this->ruta = "host='".$this->host."' port='".$this->port."' dbname='".$this->db."' user='".$this->user."' password='".$this->pass."'";
        $this->conexion=mysqli_connect($this->host,$this->user,$this->pass,$this->db);
    }
/*
*   proposito: funcion para realizar consultas 
*/
    public function consult($consulta){
        // echo $consulta;
        $this->total_consultas++;
        $this->resultado=mysqli_query($this->conexion,$consulta);
        if(!$this->resultado){
            echo 'SQL Error: ';
            exit;
        }
        return $this->resultado;
    }
/*
*   proposito: funcion para realizar insert en las tablas y retornas id 
*/
    public function save($sql, $obtenerultimoID=False){
     //   echo $sql;
        $this->resultado = mysqli_query($this->conexion,$sql);

        if($obtenerultimoID){
            $rta=False;
            if($this->resultado){
                $rta=True;
            }
            return array(
                "resultado" => $rta,
                "ultimo_id" => mysqli_fetch_object($this->resultado)
            );
        }else{
            return $this->resultado;
        }

    }
    
    function __destruct(){
        @mysqli_close($this->conexion);
    }


}

?>