<?php
require_once("conexion.php");
/*
*   fecha: 04-mayo-2021
*   autor: branm aldair pabon
*   proposito: adinistrar las tablas de las pqrs 
*/
class PQRS extends Conexion
{
    private $id;
    private $user_crea;
    private $nombre_completo;
    private $email;
    private $tipo;
    private $descri;
    private $fecha_creacion;
    private $estado;
    private $titulo;
    private $fecha_respuesta;
    private $respuesta;
    private $id_empresa;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUserCrea()
    {
        return $this->user_crea;
    }

    /**
     * @param mixed $user_crea
     *
     * @return self
     */
    public function setUserCrea($user_crea)
    {
        $this->user_crea = $user_crea;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getNombreCompleto()
    {
        return $this->nombre_completo;
    }

    /**
     * @param mixed $nombre_completo
     *
     * @return self
     */
    public function setNombreCompleto($nombre_completo)
    {
        $this->nombre_completo = $nombre_completo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     *
     * @return self
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTipo()
    {
        return $this->tipo;
    }

    /**
     * @param mixed $tipo
     *
     * @return self
     */
    public function setTipo($tipo)
    {
        $this->tipo = $tipo;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescri()
    {
        return $this->descri;
    }

    /**
     * @param mixed $descri
     *
     * @return self
     */
    public function setDescri($descri)
    {
        $this->descri = $descri;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getFechaCreacion()
    {
        return $this->fecha_creacion;
    }

    /**
     * @param mixed $fecha_creacion
     *
     * @return self
     */
    public function setFechaCreacion($fecha_creacion)
    {
        $this->fecha_creacion = $fecha_creacion;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }

    /**
     * @param mixed $estado
     *
     * @return self
     */
    public function setEstado($estado)
    {
        $this->estado = $estado;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param mixed $titulo
     *
     * @return self
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getFechaRespuesta()
    {
        return $this->fecha_respuesta;
    }

    /**
     * @param mixed $fecha_respuesta
     *
     * @return self
     */
    public function setFechaRespuesta($fecha_respuesta)
    {
        $this->fecha_respuesta = $fecha_respuesta;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getRespuesta()
    {
        return $this->respuesta;
    }

    /**
     * @param mixed $respuesta
     *
     * @return self
     */
    public function setRespuesta($respuesta)
    {
        $this->respuesta = $respuesta;

        return $this;
    }
    /**
     * @return mixed
     */
    public function getIdEmpresa()
    {
        return $this->id_empresa;
    }

    /**
     * @param mixed $id_empresa
     *
     * @return self
     */
    public function setIdEmpresa($id_empresa)
    {
        $this->id_empresa = $id_empresa;

        return $this;
    }
    
    public function __construct() {
        parent::__construct();

        $this->table = "pqrs";
    }
    /*
    * proposito: agregar una nueva pqrs asociando el usuario o cliente que este logueado
    */
    public function InsertPqrs()
    {
        try {
            $sql = "INSERT INTO $this->table 
                        (
                            user_crea,
                            nombre_completo,
                            email,
                            tipo,
                            descri,
                            fecha_creacion,
                            estado,
                            titulo,
                            id_empresa
                        )
                    VALUES 
                        (
                        $this->user_crea,
                        '$this->nombre_completo',
                        '$this->email',
                        '$this->tipo',
                        '$this->descri',
                        '$this->fecha_creacion',
                        '$this->estado',
                        '$this->titulo',
                        $this->id_empresa
                        )";
            $result = parent::save($sql);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * PROPOSITO: TRAER TODA LA INFORMACION DETALLADA DE LA PQRS CREADA POR EL USUARIO LOGUEADO PARA VER EL ESTADO
    */
    public function GetInPqrsUser()
    {
        try {
            $query = "SELECT * FROM $this->table WHERE user_crea =$this->user_crea";
            $result = parent::consult($query);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * PROPOSITO: Lista de total de PQRS pendientes por responder
    */
    public function GetAllPendientes($gerente)
    {
        try {
            $query = "SELECT p.*,e.nombre AS empresa,TIME_FORMAT(TIME(TIMEDIFF(NOW(), P.fecha_creacion)),'%H') AS diferencia
                             FROM pqrs p 
                            INNER JOIN empresa e ON p.id_empresa = e.id 
                                INNER JOIN gerente_empresa g ON g.id_empresa = e.id
                                    WHERE estado ='PENDIENTE' AND g.id_usuario = $gerente";
            $result = parent::consult($query);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * proposito: actualizar la informacion de la pqrs para pasar  un estado de realizada
    */
    public function UpdatePqrs()
    {
        try {
            $sql = "UPDATE $this->table 
                        SET respuesta = '$this->respuesta',fecha_respuesta = '$this->fecha_respuesta',estado='$this->estado'
                            WHERE id = $this->id";
            $result = parent::save($sql);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * PROPOSITO: Historico de las PQRS para que el usuario super administrador pueda ver los estados
    */
    public function GetHistorico()
    {
        try {
            $query = "SELECT p.*,
                                e.nombre AS name_empresa,
                                CASE
                                    WHEN p.fecha_respuesta IS NULL THEN 1
                                    ELSE TIME_FORMAT(TIME(TIMEDIFF(p.fecha_creacion,p.fecha_respuesta)),'%H')
                                END
                                    AS diferencia,
                                CASE
                                    WHEN p.fecha_respuesta IS NULL THEN 'PENDIENTE'
                                    ELSE p.fecha_respuesta
                                END
                                    AS fecha_respuesta_modificada
                        FROM pqrs p
                            INNER JOIN empresa e ON p.id_empresa = e.id";
            $result = parent::consult($query);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
    /*
    * PROPOSITO: Lista de total de PQRS pendientes por responder
    */
    public function GetAllEstadistica($gerente)
    {
        try {
            $query = "SELECT p.estado AS estadospqrs, COUNT(p.id) AS total
                            FROM pqrs p 
                        INNER JOIN empresa e ON p.id_empresa = e.id 
                            INNER JOIN gerente_empresa g ON g.id_empresa = e.id
                                WHERE  g.id_usuario = $gerente
                                        GROUP BY p.estado";
            $result = parent::consult($query);
            return $result;
        } catch (Exception $e) {
            echo 'Excepción capturada: ',  $e->getMessage(), "\n";
        }
    }
}
?>