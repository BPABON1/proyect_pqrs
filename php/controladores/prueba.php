<?php

/**
 *	autor: branm aldair pabon villmizar	
 *	proposito: trabajar con los modelos y realizar las validaciones respectivassss
 */
session_start();
class ControladorPrueba
{
	public function funcion($datos)
	{
		date_default_timezone_set('America/Bogota');
		$fecha_actual = date("Y-m-d H:i:s");
		/*
		*	fecha: 04-mayo-2021
		*	autor: branm aldair pabon villamizar
		*	proposito: validar la informacion del usuario que se encuentra logueado
		*/
		if ($datos->funcion === 'user_logueado') {
			try {
				require_once "modelo/usuarios.php";
				$obj_consulta = new Usuarios();
				$id = intval($_SESSION['id_usuario']);
				$obj_consulta->setId($id);
				// consultar la informacion del usuario por medio del ID
				$detalle     = $obj_consulta->GetInfoUser();
				$resultado['resultado'] = array();
				$max  = mysqli_num_rows($detalle);
				if ($max > 0) {
					$row = mysqli_fetch_object($detalle);
					$result = json_encode(array('respuesta' => true, 'result' => $row));
				} else {
					$result = json_encode(array('respuesta' => false, 'msg' => 'No se encuentran registros'));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 04-mayo-2021
		*	autor: branm aldair pabon 
		*	proposito: guardar la nueva PQRS del cliente con el estado inicial que es pendiente 
		*/
		if ($datos->funcion === 'new_pqrs') {
			try {
				require_once "modelo/pqrs.php";
				$obj_insert = new PQRS();

				$obj_insert->setUserCrea('');
				if (isset($datos->id_user_login)) {
					$obj_insert->setUserCrea($datos->id_user_login);
				}
				$obj_insert->setNombreCompleto('');
				if (isset($datos->nombre_completo)) {
					$obj_insert->setNombreCompleto($datos->nombre_completo);
				}
				if (isset($datos->id_empresa)) {
					$obj_insert->setIdEmpresa($datos->id_empresa);
				}
				$obj_insert->setEmail('');
				if (isset($datos->email)) {
					$obj_insert->setEmail(str_replace("'", '', $datos->email));
				}
				$obj_insert->setTipo('');
				if (isset($datos->tipo_pqrs)) {
					$obj_insert->setTipo($datos->tipo_pqrs);
				}
				$obj_insert->setDescri('');
				if (isset($datos->descrip_pqrs)) {
					$obj_insert->setDescri(str_replace("'", '', $datos->descrip_pqrs));
				}
				$obj_insert->setFechaCreacion($fecha_actual);
				$obj_insert->setEstado('PENDIENTE');
				$obj_insert->setTitulo('');
				if (isset($datos->titulo_pqrs)) {
					$obj_insert->setTitulo(str_replace("'", '', $datos->titulo_pqrs));
				}
				// guardar la nueva pqrs en la base de datos asociando el usuario logueado
				$insert     = $obj_insert->InsertPqrs();
				if ($insert) {
					$result = json_encode(array('respuesta' => true, 'msg' => 'Los datos se guardaron de forma correcta'));
				} else {
					$result = json_encode(array('respuesta' => False, 'msg' => 'Error al insertar los datos'));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 04-mayo-2021
		*	autor: branm aldair pabon villamizar
		*	proposito: listar las pqrs que el cliente creo para realizar seguimiento
		*/
		if ($datos->funcion === 'list_pqrs_cliente') {
			try {
				require_once "modelo/pqrs.php";
				$obj_consult = new PQRS();
				$obj_consult->setUserCrea('');
				if (isset($datos->id_user_login)) {
					$obj_consult->setUserCrea($datos->id_user_login);
				}
				$detalle     = $obj_consult->GetInPqrsUser();
				$resultado['resultado'] = array();
				$max  = mysqli_num_rows($detalle);
				if ($max > 0) {
					$cont = 0;
					while ($row = mysqli_fetch_object($detalle)) {
						$resultado['resultado'][$cont] = array(
							'id_pqrs' => $row->id,
							'user_crea' => $row->user_crea,
							'nombre_completo' => $row->nombre_completo,
							'email' => $row->email,
							'tipo' => $row->tipo,
							'descri' => $row->descri,
							'fecha_creacion' => $row->fecha_creacion,
							'estado' => $row->estado,
							'titulo' => $row->titulo,
						);
						$cont++;
					}
					$result = json_encode(array('respuesta' => true, 'result' => $resultado));
				} else {
					$result = json_encode(array('respuesta' => false, 'msg' => 'Usted no tiene PQRS creadas para realizar seguimiento.'));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 04-mayo-2021
		*	autor: branm aldair pabon villamizar
		*	proposito: agregar nuevos funcionarios para que den respuestas a las PQRS 
		*	estos usuarios tendran el rol de OPERARIO
		*/
		if ($datos->funcion === 'new_operario') {
			try {
				require_once "modelo/usuarios.php";
				$obj_consulta = new Usuarios();
				$obj_consulta->setNombres('');
				if (isset($datos->newnombre_user)) {
					$obj_consulta->setNombres(str_replace("'", '', $datos->newnombre_user));
				}
				$obj_consulta->setApellidos('');
				if (isset($datos->newapellido_user)) {
					$obj_consulta->setApellidos(str_replace("'", '', $datos->newapellido_user));
				}
				$obj_consulta->setUser('');
				if (isset($datos->newname_user)) {
					$obj_consulta->setUser(str_replace("'", '', $datos->newname_user));
				}
				if (isset($datos->newpassword)) {
					$obj_consulta->setPassword(str_replace("'", '', md5($datos->newpassword)));
				}
				$obj_consulta->setTipo('Operario');
				$obj_consulta->setEmail('');
				$obj_consulta->setUsuarioCrea($datos->id_user_login);
				$obj_consulta->setRol(3);
				if (isset($datos->newemail_user)) {
					$obj_consulta->setEmail(str_replace("'", '', $datos->newemail_user));
				}
				$obj_consulta->setDocumento('');
				if (isset($datos->newadocumento_user)) {
					$obj_consulta->setDocumento($datos->newadocumento_user);
				}
				// consultar que el nombre de usuario o numero de documento no este registrado en el sistema 
				$validacion     = $obj_consulta->ValidaUser();
				$max  = mysqli_num_rows($validacion);
				if ($max > 0) {
					$result = json_encode(array('respuesta' => False, 'msg' => 'Los datos de este usuario ya se encuentran registrados anteriormente por favor valide los datos que intenta guardar.'));
				} else {
					$query = $obj_consulta->InsertOperarios();
					if ($query) {
						$result = json_encode(array('respuesta' => True, 'msg' => 'Usuario registrado de forma correcta'));
					} else {
						$result = json_encode(array('respuesta' => false, 'msg' => 'El usuario no pudo ser registrados por favor valide la información'));
					}
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 04-mayo-2021
		*	autor: branm aldair pabon villamizar
		*	proposito: listar todos los operarios creados por el gerente
		*/
		if ($datos->funcion === 'list_operarios_gerente') {
			try {
				require_once "modelo/usuarios.php";
				$obj_consulta = new Usuarios();
				$obj_consulta->setUsuarioCrea($datos->id_user_login);
				// consultar la informacion del usuario por medio del ID
				$detalle     = $obj_consulta->GetInfoOper();
				$resultado['resultado'] = array();
				$max  = mysqli_num_rows($detalle);
				if ($max > 0) {
					$cont = 0;
					while ($row = mysqli_fetch_object($detalle)) {
						$resultado['resultado'][$cont] = array(
							'oper_id_operario' => $row->id,
							'oper_nombres' => $row->nombres,
							'oper_apellidos' => $row->apellidos,
							'oper_user' => $row->user,
							'oper_password' => $row->password,
							'oper_tipo' => $row->tipo,
							'oper_rol' => $row->rol,
							'oper_email' => $row->email,
							'oper_documento' => $row->documento,
							'oper_estado' => $row->estado,
						);
						$cont++;
					}
					$result = json_encode(array('respuesta' => true, 'result' => $resultado));
				} else {
					$result = json_encode(array('respuesta' => false, 'msg' => 'No se encuentran registros'));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 04-mayo-2021
		*	autor: branm aldair pabon villamizar
		*	proposito: actualizar la informacion del operario 
		*/
		if ($datos->funcion === 'editar_operario') {
			try {
				require_once "modelo/usuarios.php";
				$obj_consulta = new Usuarios();
				$obj_consulta->setNombres('');
				if (isset($datos->operario->oper_nombres)) {
					$obj_consulta->setNombres(str_replace("'", '', $datos->operario->oper_nombres));
				}
				$obj_consulta->setApellidos('');
				if (isset($datos->operario->oper_apellidos)) {
					$obj_consulta->setApellidos(str_replace("'", '', $datos->operario->oper_apellidos));
				}
				$obj_consulta->setId($datos->operario->oper_id_operario);
				$obj_consulta->setEmail('');
				if (isset($datos->operario->oper_email)) {
					$obj_consulta->setEmail(str_replace("'", '', $datos->operario->oper_email));
				}
				if (isset($datos->operario->oper_estado)) {
					$obj_consulta->setEstado($datos->operario->oper_estado);
				}
				$query = $obj_consulta->UpdateOperarios();
				if ($query) {
					$result = json_encode(array('respuesta' => True, 'msg' => 'Usuario actualizado de forma correcta'));
				} else {
					$result = json_encode(array('respuesta' => false, 'msg' => 'El usuario no pudo ser actualizado por favor valide la información'));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 04-mayo-2021
		*	autor: branm aldair pabon
		*	proposito. listar el total de empresas en el sistema para asociar a pqrs
		*/
		if ($datos->funcion === 'list_empresas') {
			try {
				require_once "modelo/empresas.php";
				$obj_consulta = new EMPRESAS();
				$detalle     = $obj_consulta->GetAll();
				$resultado['resultado'] = array();
				$max  = mysqli_num_rows($detalle);
				if ($max > 0) {
					$cont = 0;
					while ($row = mysqli_fetch_object($detalle)) {
						$resultado['resultado'][$cont] = array(
							'id_empresa' => $row->id,
							'nombre_empresa' => $row->nombre,
							'estado_empresa' => $row->estado_empresa
						);
						$cont++;
					}
					$result = json_encode(array('respuesta' => true, 'result' => $resultado));
				} else {
					$result = json_encode(array('respuesta' => false, 'msg' => 'No se encuentran registros'));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 04-mayo-2021
		*	autor: branm aldair pabon villamizar
		*	proposito: buscar todas las pqrs pendientes por realizar
		*/
		if ($datos->funcion === 'list_pqrs_pendiente') {
			try {
				require_once "modelo/pqrs.php";
				$obj_consult = new PQRS();
				$detalle     = $obj_consult->GetAllPendientes(intval($datos->usuario_crea));
				$resultado['resultado'] = array();
				$max  = mysqli_num_rows($detalle);
				if ($max > 0) {
					$cont = 0;
					while ($row = mysqli_fetch_object($detalle)) {
						$resultado['resultado'][$cont] = array(
							'id_pqrs' => $row->id,
							'user_crea' => $row->user_crea,
							'nombre_completo' => $row->nombre_completo,
							'email' => $row->email,
							'tipo' => $row->tipo,
							'descri' => $row->descri,
							'fecha_creacion' => $row->fecha_creacion,
							'estado' => $row->estado,
							'titulo' => $row->titulo,
							'name_empresa' => $row->empresa,
							'diferencia' => intval($row->diferencia)
						);
						$cont++;
					}
					$result = json_encode(array('respuesta' => true, 'result' => $resultado));
				} else {
					$result = json_encode(array('respuesta' => false, 'msg' => 'No se encuentran PQRS pendientes por ser gestionadas'));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 04-mayo-2021
		*	autor: branm aldair pabon villamizar
		*	proposito: actualizar el estado de la pqrs y poner una fecha de realizacion y su respectiva respuesta
		*/
		if ($datos->funcion === 'respuesta_pqrs') {
			try {
				require_once "modelo/pqrs.php";
				$obj_update = new PQRS();

				$obj_update->setId('');
				if (isset($datos->respuestapqrs->id_pqrs)) {
					$obj_update->setId($datos->respuestapqrs->id_pqrs);
				}
				$obj_update->setRespuesta('');
				if (isset($datos->respuestapqrs->respuesta)) {
					$obj_update->setRespuesta($datos->respuestapqrs->respuesta);
				}
				$obj_update->setFechaRespuesta($fecha_actual);
				$obj_update->setEstado('REALIZADA');
				// actualizar los datos de la pqrs para pasar a un estado de realizado
				$insert     = $obj_update->UpdatePqrs();
				if ($insert) {
					$result = json_encode(array('respuesta' => true, 'msg' => 'Los datos se actualizaron de forma correcta'));
				} else {
					$result = json_encode(array('respuesta' => False, 'msg' => 'Error al actualizar los datos'));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 04-mayo-2021
		*	autor: branm aldair pabon villamizar
		*	proposito: validar que la empresa no este asociada a algun gerente para su gestion
		*/
		if ($datos->funcion === 'valida_empresa_gerente') {
			try {
				require_once "modelo/gerente_empresas.php";
				$obj_consult = new GERENTEEMPRESAS();

				if (isset($datos->Gereid_empresa)) {
					$obj_consult->setIdEmpresa($datos->Gereid_empresa);
				}
				$consult     = $obj_consult->ValidaEmpresa();
				$max  = mysqli_num_rows($consult);
				if ($max > 0) {
					$result = json_encode(array('respuesta' => False, 'msg' => 'Esta empresa ya tiene un gerente asignado por favor asocie a otra empresa '));
				} else {
					$result = json_encode(array('respuesta' => True));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 05-mayo-2021
		*	autor: branm aldair pabon villamizar
		*	proposito: crear un nuevo usuario con el rol gerente y asociar  una empresa
		*/
		if ($datos->funcion === 'crear_gerente') {
			try {
				require_once "modelo/usuarios.php";
				require_once "modelo/gerente_empresas.php";
				$obj_consulta = new Usuarios();
				$obj_gerente_empresa = new GERENTEEMPRESAS();
				$obj_consulta->setNombres('');
				if (isset($datos->Gerenombre_user)) {
					$obj_consulta->setNombres(str_replace("'", '', $datos->Gerenombre_user));
				}
				$obj_consulta->setApellidos('');
				if (isset($datos->Gereapellido_user)) {
					$obj_consulta->setApellidos(str_replace("'", '', $datos->Gereapellido_user));
				}
				$obj_consulta->setUser('');
				if (isset($datos->Gerename_user)) {
					$obj_consulta->setUser(str_replace("'", '', $datos->Gerename_user));
				}
				if (isset($datos->Gerepassword)) {
					$obj_consulta->setPassword(str_replace("'", '', md5($datos->Gerepassword)));
				}
				$obj_consulta->setTipo('GERENTE');
				$obj_consulta->setRol(2);
				$obj_consulta->setEmail('');
				if (isset($datos->Gereemail_user)) {
					$obj_consulta->setEmail(str_replace("'", '', $datos->Gereemail_user));
				}
				$obj_consulta->setDocumento('');
				if (isset($datos->Gereadocumento_user)) {
					$obj_consulta->setDocumento($datos->Gereadocumento_user);
				}
				// consultar que el nombre de usuario o numero de documento no este registrado en el sistema 
				$validacion     = $obj_consulta->ValidaUser();
				$max  = mysqli_num_rows($validacion);
				if ($max > 0) {
					$result = json_encode(array('respuesta' => False, 'msg' => 'Los datos de este usuario ya se encuentran registrados anteriormente por favor valide los datos que intenta guardar.'));
				} else {
					$query = $obj_consulta->InsertGerente();
					$detalle_user     = $obj_consulta->ValidaUser();
					$max2  = mysqli_num_rows($detalle_user);
					if ($max2 > 0) {
						$ultimo_id   = $row = mysqli_fetch_object($detalle_user);
						// recopilar la informacion para asociar la empresa al gernete 
						if (isset($datos->Gereid_empresa)) {
							$obj_gerente_empresa->setIdEmpresa($datos->Gereid_empresa);
						}
						if (isset($ultimo_id->id)) {
							$obj_gerente_empresa->setIdUsuario($ultimo_id->id);
						}
						$rta = $obj_gerente_empresa->InsertRelacion();
						if ($rta) {
							$result = json_encode(array('respuesta' => True, 'msg' => 'Usuario registrado de forma correcta'));
						} else {
							$result = json_encode(array('respuesta' => False, 'msg' => 'El usuario no pudo ser relacionado con la empresa'));
						}
					} else {
						$result = json_encode(array('respuesta' => false, 'msg' => 'El usuario no pudo ser registrados por favor valide la información'));
					}
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 05-mayo-2021
		*	autor: branm aldair pabon villamizar
		*	proposito: listar el total de usuarios por tipos gerentes para la administracion
		*/
		if ($datos->funcion === 'list_gerente_empresas') {
			try {
				require_once "modelo/usuarios.php";
				require_once "modelo/empresas.php";
				$obj_consulta = new Usuarios();
				$obj_empreesa = new EMPRESAS();
				// consultar la informacion del usuario tipo gerentes
				$detalle     = $obj_consulta->GetInfoGerente();
				$resultado['resultado'] = array();
				$max  = mysqli_num_rows($detalle);
				if ($max > 0) {
					$cont = 0;
					while ($row = mysqli_fetch_object($detalle)) {
						$resultado['resultado'][$cont] = array(
							'id_usuari' => $row->id_usuari,
							'apellidos_user' => $row->apellidos_user,
							'nombres_user' => $row->nombres_user,
							'user_u' => $row->user_u,
							'user_tipo' => $row->user_tipo,
							'user_rol' => $row->user_rol,
							'user_email' => $row->user_email,
							'user_estado' => $row->user_estado,
							'id_empresa_guardada' => $row->id_empresa,
							'id_nombre_empresa' => $row->id_nombre_empresa,
						);
						$cont++;
					}
					$empresas     = $obj_empreesa->GetAll();
					$max2  = mysqli_num_rows($empresas);
					if ($max2 > 0) {
						$contador = 0;
						while ($row = mysqli_fetch_object($empresas)) {
							$resultado['empresa'][$contador] = array(
								'id_empresa' => $row->id,
								'nombre_empresa' => $row->nombre
							);
							$contador++;
						}
					}
					$result = json_encode(array('respuesta' => true, 'result' => $resultado));
				} else {
					$result = json_encode(array('respuesta' => false, 'msg' => 'No se encuentran registros'));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 05-mayo-2021
		*	autor: branm aldair pabon 
		*	proposito: editar los usuarios de tipo gerentes
		*/
		if($datos->funcion === 'editar_gerente'){
			try {
				require_once "modelo/usuarios.php";
				$obj_consulta = new Usuarios();
				$obj_consulta->setNombres('');
				if (isset($datos->gerente->nombres_user)) {
					$obj_consulta->setNombres(str_replace("'", '', $datos->gerente->nombres_user));
				}
				$obj_consulta->setApellidos('');
				if (isset($datos->gerente->apellidos_user)) {
					$obj_consulta->setApellidos(str_replace("'", '', $datos->gerente->apellidos_user));
				}
				$obj_consulta->setId($datos->gerente->id_usuari);
				$obj_consulta->setEmail('');
				if (isset($datos->gerente->user_email)) {
					$obj_consulta->setEmail(str_replace("'", '', $datos->gerente->user_email));
				}
				if (isset($datos->gerente->user_estado)) {
					$obj_consulta->setEstado($datos->gerente->user_estado);
				}
				$query = $obj_consulta->UpdateOperarios();
				if ($query) {
					$result = json_encode(array('respuesta' => True, 'msg' => 'Usuario actualizado de forma correcta'));
				} else {
					$result = json_encode(array('respuesta' => false, 'msg' => 'El usuario no pudo ser actualizado por favor valide la información'));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 05-mayo-2021
		*	autor: branm aldair pabon 
		*	proposito: listar el historial de las pqrs 
		*/
		if($datos->funcion === 'list_pqrs_historico'){
			try {
				require_once "modelo/pqrs.php";
				$obj_consult = new PQRS();
				$detalle     = $obj_consult->GetHistorico();
				$resultado['resultado'] = array();
				$max  = mysqli_num_rows($detalle);
				if ($max > 0) {
					$cont = 0;
					while ($row = mysqli_fetch_object($detalle)) {
						$resultado['resultado'][$cont] = array(
							'id_pqrs' => $row->id,
							'user_crea' => $row->user_crea,
							'nombre_completo' => $row->nombre_completo,
							'email' => $row->email,
							'tipo' => $row->tipo,
							'descri' => $row->descri,
							'fecha_creacion' => $row->fecha_creacion,
							'estado' => $row->estado,
							'titulo' => $row->titulo,
							'fecha_respuesta_modificada' => $row->fecha_respuesta_modificada,
							'respuesta' => $row->respuesta,
							'diferencia' => $row->diferencia,
							'name_empresa' => $row->name_empresa
						);
						$cont++;
					}
					$result = json_encode(array('respuesta' => true, 'result' => $resultado));
				} else {
					$result = json_encode(array('respuesta' => false, 'msg' => 'No se encuentran PQRS pendientes por ser gestionadas'));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}	
		}
		/*
		*	fecha: 10-mayo-2021
		*	autor: branm aldair pabon villamizar
		*	proposito: agregar las nuevas empresas
		*/
		if($datos->funcion === 'new_empresa'){
			try {
				require_once "modelo/empresas.php";
				$obj_empresa = new EMPRESAS();
				$obj_empresa->setNombre('');
				if(isset($datos->NewEmpresa)){
					$obj_empresa->setNombre(str_replace("'", '', $datos->NewEmpresa));
				}
				$insert     = $obj_empresa->InsertEmpresa();
				if ($insert) {
					$result = json_encode(array('respuesta' => true, 'msg' => 'La empresa se guardo de forma correcta'));
				} else {
					$result = json_encode(array('respuesta' => false, 'msg' => 'No se guardo la empresa'));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 10-mayo-2021
		*	autor. branm aldair pabon villamizar
		*	proposito: editar los valores de la empresa
		*/
		if($datos->funcion === 'edit_empresa'){
			try {
				require_once "modelo/empresas.php";
				$obj_empresa = new EMPRESAS();
				if(isset($datos->editempresa->id_empresa)){
					$obj_empresa->setNombre('');
					if(isset($datos->editempresa->nombre_empresa)){
						$obj_empresa->setNombre(str_replace("'", '', $datos->editempresa->nombre_empresa));
					}
					$obj_empresa->setId($datos->editempresa->id_empresa);
					if(isset($datos->editempresa->estado_empresa)){
						$obj_empresa->setEstadoEmpresa($datos->editempresa->estado_empresa);
					}
					$update     = $obj_empresa->UpdateEmpresa();
					if ($update) {
						$result = json_encode(array('respuesta' => true, 'msg' => 'Datos actualizados de forma correcta'));
					} else {
						$result = json_encode(array('respuesta' => false, 'msg' => 'No se edito la empresa'));
					}
				}
				else{
					$result = json_encode(array('respuesta' => false, 'msg' => 'Verifique los datos de la empresa que desea editar'));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 10-mayo-2021
		*	autor: branm aldair pabon villamizar
		*	proposito: eliminar la empresa
		*/
		if($datos->funcion === 'delete_empresa'){
			try {
				require_once "modelo/empresas.php";
				$obj_empresa = new EMPRESAS();
				if(isset($datos->editempresa->id_empresa)){
					$obj_empresa->setId($datos->editempresa->id_empresa);
					$update     = $obj_empresa->DeleteEmpresa();
					if ($update) {
						$result = json_encode(array('respuesta' => true, 'msg' => 'Datos eliminados de forma correcta'));
					} else {
						$result = json_encode(array('respuesta' => false, 'msg' => 'No se elimino la empresa'));
					}
				}
				else{
					$result = json_encode(array('respuesta' => false, 'msg' => 'Verifique los datos de la empresa que desea eliminar'));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 04-mayo-2021
		*	autor: branm aldair pabon
		*	proposito. listar el total de empresas activas o inactivas en el sistema
		*/
		if ($datos->funcion === 'list_total_empresas') {
			try {
				require_once "modelo/empresas.php";
				$obj_consulta = new EMPRESAS();
				$detalle     = $obj_consulta->GetAllTotal();
				$resultado['resultado'] = array();
				$max  = mysqli_num_rows($detalle);
				if ($max > 0) {
					$cont = 0;
					while ($row = mysqli_fetch_object($detalle)) {
						$resultado['resultado'][$cont] = array(
							'id_empresa' => $row->id,
							'nombre_empresa' => $row->nombre,
							'estado_empresa' => $row->estado_empresa
						);
						$cont++;
					}
					$result = json_encode(array('respuesta' => true, 'result' => $resultado));
				} else {
					$result = json_encode(array('respuesta' => false, 'msg' => 'No se encuentran registros'));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		/*
		*	fecha: 10-mayo-2021
		*	autor: branm aldair pabon villamizar
		*	proposito: listar las pqrs que tiene el gerente con el fin de realizar una grafica
		*/
		if ($datos->funcion === 'list_estadistica_pqrs') {
			try {
				require_once "modelo/pqrs.php";
				$obj_consult = new PQRS();
				$detalle     = $obj_consult->GetAllEstadistica($datos->id_user_login);
				$max  = mysqli_num_rows($detalle);
				if ($max > 0) {
					$pendiente = 0;
					$realizada = 0;
					while ($row = mysqli_fetch_object($detalle)) {
						if($row->estadospqrs == 'PENDIENTE'){
							$pendiente = $row->total;
						}
						else if($row->estadospqrs == 'REALIZADA'){
							$realizada = $row->total;
						}
					}
					$result = json_encode(array('respuesta' => true, 'pendiente' => $pendiente,'realizada'=>$realizada));
				} else {
					$result = json_encode(array('respuesta' => false, 'msg' => 'No se tiene información para mostrar '));
				}
			} catch (Exception $e) {
				$result = json_encode(array('respuesta' => false, 'msg' => 'Error por favor comuníquese con el administrador del sistema'));
			}
		}
		echo ($result);
	}
}
