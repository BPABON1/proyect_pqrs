    <?php
ini_set("display_errors", "off");
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Methods: POST, GET, OPTIONS");
header("Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With");
header("Content-Type: application/json; charset=utf-8");

$request = json_decode(file_get_contents("php://input"));
$resultado = array();
if (isset($request)) {
    if ($request->datos->modelo === 'prueba') {
        require_once 'controladores/prueba.php';
        $controlador = new ControladorPrueba();
        $resultado   = $controlador->funcion($request->datos);
    }
}
?>