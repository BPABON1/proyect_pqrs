var app = angular.module('myApp', ['ngRoute', 'ngResource', 'ui.router', 'oc.lazyLoad']);
/*
 *   fecha: 04-05-2021
 *   proposito: realizar la configuracion respectiva de los controladores y de las funciones a utilizar
 */
app.config(['$stateProvider', '$urlRouterProvider', '$ocLazyLoadProvider', function($stateProvider, $urlRouterProvider, $ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        debug: true,
        events: true
    });
    /*
     * fecha: 04-mayo-2021
     * proposito: redireccionar a las vistas establecidas por el controller y si es necesario pasar informacion por la url
     */
    $urlRouterProvider.otherwise('/home');
    $urlRouterProvider.when('', '/home');
    $stateProvider
        .state('home', {
            templateUrl: 'index.php',
            url: '/inicio',
            controller: 'controller_home',
            resolve: {
                loadMyFile: function($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'app',
                        files: [
                            'JS/controller_home.js'
                        ],
                        cache: false
                    })
                }
            },
        })
}]);
app.controller('myCtrl', function($scope, $http, $state) {
    var url = 'http://localhost/proyect_pqrs/php/general_controller.php';
    $scope.objeto = {};
    $scope.temporal = {};
    /*
     * fecha: 03-mayo-2021
     * proposito: Abrior el modal de registro de usuarios de clientes
     */
    $scope.abrir = function() {
        $('#exampleModal').modal('show')
    }
});