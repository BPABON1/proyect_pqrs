angular.module('myApp').controller('controller_home', function($scope, $http) {
    var url = 'http://localhost/proyect_pqrs/php/general_controller.php';
    $scope.objeto = {};
    $scope.temporal = {};
    $scope.objeto.PagesnewEmpresa = 'NO';
    $scope.temporalpqrs = {};
    $scope.temporalEmpresas = {};
    $scope.temporalOperarios = {};
    $scope.temporalPqrsPendientes = {};
    $scope.temporalGerentes = {};
    $scope.temporalHistorico = {};
    $scope.temporaltotalempresas = {};
    /*
     * fecha: 03-mayo-2021
     * proposito: validar en el servidor los datos del usuario que se esta logueando 
     */
    $scope.LoginServidor = function() {
        Swal.fire({
            title: '<strong>Cargando...<u></u></strong>',
            allowOutsideClick: false,
            timerProgressBar: true,
            showCloseButton: false,
            showCancelButton: false,
        });
        $scope.objeto.modelo = 'prueba';
        $scope.objeto.funcion = 'user_logueado';
        var datos = { datos: $scope.objeto };
        $http.post(url, datos).then(function(response) {
            if (response.data.respuesta) {
                Swal.close();
                $scope.objeto = response.data.result;
                $scope.objeto.vista = 'INICIO';
                $scope.objeto.id_user_login = response.data.result.id;
                $scope.objeto.nombre_completo = $scope.objeto.nombres + ' ' + $scope.objeto.apellidos;
                console.log($scope.objeto);
            } else {
                $scope.objeto = {};
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: response.data.msg,
                })
            }
        }, function(response) {
            $scope.errorAjax(response, "Se ha presentado un error");
        });
    };
    $scope.LoginServidor();
    /*
     *   fecha: 04-mayo-2021
     *   autor: branm aldair pabon
     *   proposito: guardar todos los datos de la pqrs asociado al id del usuario logueado
     */
    $scope.CrearPqrs = function() {
            Swal.fire({
                title: '<strong>Guardando...<u></u></strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'new_pqrs';
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        Swal.fire({
                            type: 'success',
                            title: 'Exito',
                            text: response.data.msg,
                        });
                        $scope.objeto.titulo_pqrs = null;
                        $scope.objeto.tipo_pqrs = null;
                        $scope.objeto.descrip_pqrs = null;
                    } else {
                        $scope.objeto = {};
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 04-mayo-2021
         *   autor: branm aldair pabon
         *   proposito: listar las pqrs que el usuario creo para realizar seguimiento de las PQRS creadas
         */
    $scope.ListarPqrs = function() {
            Swal.fire({
                title: '<strong>Buscando...<u></u></strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'list_pqrs_cliente';
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        $scope.temporalpqrs = {};
                        $scope.temporalpqrs = response.data.result.resultado;
                        Swal.close();
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 04-mayo-2021
         *   autor: branm aldair pabon villamizar
         *   proposito: crear los nuevos operario dependiendo del usuario que este logueado
         */
    $scope.CrearOperario = function() {
            Swal.fire({
                title: '<strong>Guardando...<u></u></strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'new_operario';
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        Swal.fire({
                            type: 'success',
                            title: 'Exito',
                            text: response.data.msg,
                        });
                        $scope.objeto.newnombre_user = null;
                        $scope.objeto.newapellido_user = null;
                        $scope.objeto.newadocumento_user = null;
                        $scope.objeto.newemail_user = null;
                        $scope.objeto.newname_user = null;
                        $scope.objeto.newpassword = null;
                    } else {
                        $scope.objeto.newnombre_user = null;
                        $scope.objeto.newapellido_user = null;
                        $scope.objeto.newadocumento_user = null;
                        $scope.objeto.newemail_user = null;
                        $scope.objeto.newname_user = null;
                        $scope.objeto.newpassword = null;
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 04-mayo-2021
         *   autor: branm aldair pabon
         *   proposito: listar todos los operarios creados por el gerente para su administracion
         */
    $scope.ListarOperarios = function() {
            Swal.fire({
                title: '<strong>Buscando...<u></u></strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'list_operarios_gerente';
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        $scope.temporalOperarios = {};
                        $scope.temporalOperarios = response.data.result.resultado;
                        Swal.close();
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 04-mayo-2021
         *   autor: branm aldair pabon villamizar
         *   proposito: actualizar la informacion de los usuarios tipo operarios creados 
         */
    $scope.EditarOperario = function(info) {
            Swal.fire({
                title: '<strong>Guardando...<u></u></strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'editar_operario';
            $scope.objeto.operario = info;
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        Swal.fire({
                            type: 'success',
                            title: 'Exito',
                            text: response.data.msg,
                        });
                        setTimeout(() => {
                            $scope.ListarOperarios();
                        }, 1500);
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 04-mayo-2021
         *   autor: branm aldair pabon villamizar
         *   proposito: mostrar el total de empresas que se tiene 
         */
    $scope.ListarEmpresas = function() {
            Swal.fire({
                title: '<strong>Buscando...<u></u></strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'list_empresas';
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        $scope.temporalEmpresas = {};
                        $scope.temporalEmpresas = response.data.result.resultado;
                        Swal.close();
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 04-mayo-2021
         *   autor: branm aldair pabon villamizar
         *   proposito: listar el total de PQRS pendientes por responder
         */
    $scope.ListarpqrsPendientes = function() {
            Swal.fire({
                title: '<strong>Buscando...<u></u></strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'list_pqrs_pendiente';
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        $scope.temporalPqrsPendientes = {};
                        $scope.temporalPqrsPendientes = response.data.result.resultado;
                        Swal.close();
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                        $scope.objeto.vista = 'INICIO';
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 04-mayo-2021
         *   autor: branm aldair pabon 
         *   proposito: almacenar en varibable temporal la informacion de la pqrs para ser respondida en un modal
         */
    $scope.ResponderPqrs = function(item) {
            console.log(item);
            if (!item.respuesta) {
                item.respuesta = [];
            }
            if (item.respuesta.length > 0) {
                Swal.fire({
                    title: '<strong>Actualizando...<u></u></strong>',
                    allowOutsideClick: false,
                    timerProgressBar: true,
                    showCloseButton: false,
                    showCancelButton: false,
                });
                $scope.objeto.modelo = 'prueba';
                $scope.objeto.funcion = 'respuesta_pqrs';
                $scope.objeto.respuestapqrs = item;
                var datos = { datos: $scope.objeto };
                if ($scope.objeto.id_user_login) {
                    $http.post(url, datos).then(function(response) {
                        if (response.data.respuesta) {
                            Swal.fire({
                                type: 'success',
                                title: 'Exito',
                                text: response.data.msg,
                            });
                            setTimeout(() => {
                                $scope.ListarpqrsPendientes();
                            }, 1500);
                        } else {
                            $scope.objeto = {};
                            Swal.fire({
                                type: 'error',
                                title: 'Error',
                                text: response.data.msg,
                            })
                        }
                    }, function(response) {
                        $scope.errorAjax(response, "Se ha presentado un error");
                    });
                } else {
                    Swal.fire({
                        type: 'error',
                        title: 'Error',
                        text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                    })
                }
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Se debe ingresar una respuesta valida para guardar la información ',
                })
            }
        }
        /*
         *   fecha: 04-mayo-2021
         *   autor: branm aldair pabon villamizar
         *   proposito: Guardar los usuarios tipo gerente
         */
    $scope.GuardarGerentes = function() {
            Swal.fire({
                title: '<strong>Guardando...<u></u></strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'crear_gerente';
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        Swal.fire({
                            type: 'success',
                            title: 'Exito',
                            text: response.data.msg,
                        });
                        $scope.objeto.Gerenombre_user = null;
                        $scope.objeto.Gereapellido_user = null;
                        $scope.objeto.Gereadocumento_user = null;
                        $scope.objeto.Gereemail_user = null;
                        $scope.objeto.Gerename_user = null;
                        $scope.objeto.Gereid_empresa = null;
                        $scope.objeto.Gerepassword = null;
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 05-mayo-2021
         *   autor: branm aldair pabon villamizar
         *   proposito: validar que esa empresa seleccionada no tenga un gerente ya asociado
         */
    $scope.ValidarAsociacionEmpresa = function() {
            Swal.fire({
                title: '<strong>Validando información...<u></u></strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'valida_empresa_gerente';
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        console.log('validacion correcta');
                        Swal.close();
                    } else {
                        $scope.objeto.Gereid_empresa = null;
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 05-mayo-2021
         *   autor: branm aldair pabon villamizar
         *   proposito: listar el total de usuarios tipo gerente para administrar los usuarios
         */
    $scope.ListarGerentes = function() {
            Swal.fire({
                title: '<strong>Buscando...<u></u></strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'list_gerente_empresas';
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        $scope.temporalGerentes = {};
                        $scope.temporalGerentes = response.data.result.resultado;
                        Swal.close();
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 05-mayo-2021
         *   autor: branm aldair pabon
         *   proposito: editar los usuarios tipo gerentes
         */
    $scope.EditarGerente = function(info) {
            Swal.fire({
                title: '<strong>Guardando...<u></u></strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'editar_gerente';
            $scope.objeto.gerente = info;
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        Swal.fire({
                            type: 'success',
                            title: 'Exito',
                            text: response.data.msg,
                        });
                        setTimeout(() => {
                            $scope.ListarGerentes();
                        }, 1500);
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 05-mayo-2021
         *   autor: branm aldair pabon villamizar
         *   proposito: listar el historico de las pqrs
         */
    $scope.ListarHistoricoPqrs = function() {
            Swal.fire({
                title: '<strong>Buscando...</strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'list_pqrs_historico';
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        $scope.temporalHistorico = {};
                        $scope.temporalHistorico = response.data.result.resultado;
                        Swal.close();
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 10-mayo-2021
         *   autor: branm aldair pabon villamizar
         *   proposito: listar el total de empresas
         */
    $scope.GuardarNewEmpresa = function() {
            Swal.fire({
                title: '<strong>Guardando...<u></u></strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'new_empresa';
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        Swal.fire({
                            type: 'success',
                            title: 'Exito',
                            text: response.data.msg,
                        });
                        $scope.objeto.PagesnewEmpresa = 'NO';
                        $scope.objeto.NewEmpresa = null;
                        setTimeout(() => {
                            $scope.ListarTotalEmpresas();
                        }, 1500);
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 10-mayo-2021
         *   autor: branm aldair pabon villamizar
         *   proposito: editar el nombre de la empresa
         */
    $scope.EditarEmpresa = function(item) {
            Swal.fire({
                title: '<strong>Guardando...<u></u></strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'edit_empresa';
            $scope.objeto.editempresa = item;
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        Swal.fire({
                            type: 'success',
                            title: 'Exito',
                            text: response.data.msg,
                        });
                        setTimeout(() => {
                            $scope.ListarTotalEmpresas();
                        }, 1500);
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 10-mayo-2021
         *   autor: branm aldair pabon villamizar
         *   proposito: eliminar empresa 
         */
    $scope.EliminarEmpresa = function(item) {
            Swal.fire({
                title: '<strong>Guardando...<u></u></strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'delete_empresa';
            $scope.objeto.editempresa = item;
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        Swal.fire({
                            type: 'success',
                            title: 'Exito',
                            text: response.data.msg,
                        });
                        setTimeout(() => {
                            $scope.ListarEmpresas();
                        }, 1500);
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 04-mayo-2021
         *   autor: branm aldair pabon villamizar
         *   proposito: mostrar el total de empresas que se tiene 
         */
    $scope.ListarTotalEmpresas = function() {
            Swal.fire({
                title: '<strong>Buscando...<u></u></strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'list_total_empresas';
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        $scope.temporalEmpresas = {};
                        $scope.temporalEmpresas = response.data.result.resultado;
                        Swal.close();
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 10-mayo-2021
         *   autor: branm aldair pabon villamizar
         *   proposito: Cargar todas el total de pqrs pendientes y de realizadas para graficar 
         */
    $scope.BuscarEstadistica = function() {
            Swal.fire({
                title: '<strong>Buscando...<u></u></strong>',
                allowOutsideClick: false,
                timerProgressBar: true,
                showCloseButton: false,
                showCancelButton: false,
            });
            $scope.objeto.modelo = 'prueba';
            $scope.objeto.funcion = 'list_estadistica_pqrs';
            var datos = { datos: $scope.objeto };
            if ($scope.objeto.id_user_login) {
                $http.post(url, datos).then(function(response) {
                    if (response.data.respuesta) {
                        $scope.GraficoGoogle(parseInt(response.data.pendiente), parseInt(response.data.realizada));
                        Swal.close();
                    } else {
                        Swal.fire({
                            type: 'error',
                            title: 'Error',
                            text: response.data.msg,
                        })
                    }
                }, function(response) {
                    $scope.errorAjax(response, "Se ha presentado un error");
                });
            } else {
                Swal.fire({
                    type: 'error',
                    title: 'Error',
                    text: 'Datos de usuario incorrectos valide que tiene una sesión activa',
                })
            }
        }
        /*
         *   fecha: 10-mayo-2021
         *   autor: branm aldair pabon villamizar    
         *   proposito: cargar el grafico de google 
         */
    $scope.GraficoGoogle = function(pendiente, realizadas) {
        google.charts.load('current', { 'packages': ['corechart'] });
        google.charts.setOnLoadCallback(drawChart);

        // Draw the chart and set the chart values
        function drawChart() {
            var data = google.visualization.arrayToDataTable([
                ['Tipo', 'Total'],
                ['Pendientes', pendiente],
                ['Realizadas', realizadas]
            ]);
            var options = { 'title': 'Total, estados PQRS', 'width': 550, 'height': 400 };
            var chart = new google.visualization.PieChart(document.getElementById('piechart'));
            chart.draw(data, options);
        }
    }
});