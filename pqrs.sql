-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-05-2021 a las 02:05:33
-- Versión del servidor: 10.4.18-MariaDB
-- Versión de PHP: 8.0.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `pqrs`
--
CREATE DATABASE IF NOT EXISTS `pqrs` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci;
USE `pqrs`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

CREATE TABLE `empresa` (
  `id` int(11) NOT NULL,
  `nombre` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `estado_empresa` varchar(12) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `empresa`
--

INSERT INTO `empresa` (`id`, `nombre`, `estado_empresa`) VALUES
(1, 'MARVAL', 'ACTIVO'),
(5, 'YUPI', 'INACTIVO'),
(6, 'COCA COLA', 'ACTIVO'),
(7, 'POSTOBON', 'ACTIVO'),
(8, 'FOSCAL', 'ACTIVO'),
(9, 'APPLE', 'ACTIVO'),
(10, 'COPETRAN', 'ACTIVO'),
(11, 'MOTOROLA', 'ACTIVO'),
(12, 'NOKIA', 'ACTIVO'),
(13, 'CLARO', 'ACTIVO'),
(14, 'MOVISTAR', 'ACTIVO'),
(15, 'STUDIO F', 'ACTIVO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gerente_empresa`
--

CREATE TABLE `gerente_empresa` (
  `id` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `gerente_empresa`
--

INSERT INTO `gerente_empresa` (`id`, `id_usuario`, `id_empresa`) VALUES
(1, 3, 1),
(3, 8, 5),
(4, 9, 12),
(5, 12, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pqrs`
--

CREATE TABLE `pqrs` (
  `id` int(11) NOT NULL,
  `user_crea` int(11) NOT NULL,
  `nombre_completo` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `descri` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `estado` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `titulo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `fecha_respuesta` datetime NOT NULL,
  `respuesta` text COLLATE utf8_spanish_ci NOT NULL,
  `id_empresa` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `pqrs`
--

INSERT INTO `pqrs` (`id`, `user_crea`, `nombre_completo`, `email`, `tipo`, `descri`, `fecha_creacion`, `estado`, `titulo`, `fecha_respuesta`, `respuesta`, `id_empresa`) VALUES
(5, 11, 'CLIENTE PRUEBA', 'cliente@gmail.com', 'PETICION', 'REALIZAR CAMBIO DE PERSONAL', '2021-05-05 10:43:58', 'REALIZADA', 'PRUEBA PQRS', '2021-05-05 10:47:15', 'NO SE PUEDE REALIZAR CAMBIO DE PERSONAL', 1),
(6, 11, 'CLIENTE PRUEBA', 'cliente@gmail.com', 'RECLAMO', 'PRUEBA PARA ESTADISTICAS PQRS', '2021-05-10 17:40:22', 'PENDIENTE', 'RECLAMO', '0000-00-00 00:00:00', '', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `nombre`) VALUES
(1, 'CLIENTE'),
(2, 'GERENTE'),
(3, 'OPERARIO'),
(4, 'ROOT');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nombres` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `apellidos` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `user` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `rol` int(2) NOT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `documento` int(15) NOT NULL,
  `usuario_crea` int(11) NOT NULL,
  `estado` varchar(11) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla creada para almacenar los usuarios tanto inter e exter';

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`id`, `nombres`, `apellidos`, `user`, `password`, `tipo`, `rol`, `email`, `documento`, `usuario_crea`, `estado`) VALUES
(3, 'GERENTE', 'NACIONAL', 'GERENTE', '827ccb0eea8a706c4c34a16891f84e7b', 'GERENTE', 2, 'gerente@gmail.com', 123456, 0, 'ACTIVO'),
(4, 'JUAN', 'PEREZ', 'JUAN', '827ccb0eea8a706c4c34a16891f84e7b', 'Operario', 3, 'JUAN@GMAIL.COM', 1234567, 3, 'ACTIVO'),
(5, 'USUARIO', 'ADMINISTRADOR', 'ROOT', '827ccb0eea8a706c4c34a16891f84e7b', 'ADMINISTRADOR', 4, 'root@gmail.com', 123456789, 0, 'ACTIVO'),
(8, 'CARLOS', 'SANCHEZ', 'gerente2', '827ccb0eea8a706c4c34a16891f84e7b', 'GERENTE', 2, 'carlos@gmail.com', 2123213, 0, 'INACTIVO'),
(9, 'ALDAIR', 'PABON', 'aldair', '827ccb0eea8a706c4c34a16891f84e7b', 'GERENTE', 2, 'aldair@gmail.com', 1232321, 0, 'INACTIVO'),
(11, 'CLIENTE', 'PRUEBA', 'CLIENTE', '827ccb0eea8a706c4c34a16891f84e7b', 'Cliente', 1, 'cliente@gmail.com', 13223212, 0, 'ACTIVO'),
(12, 'FOSCAL', 'PRUEBA GERENTE', 'FOSCAL', '827ccb0eea8a706c4c34a16891f84e7b', 'GERENTE', 2, 'foscal@gmail.com', 132321321, 0, 'ACTIVO'),
(13, 'PQRS', 'PRUEBA PQRS', 'PQRS', '827ccb0eea8a706c4c34a16891f84e7b', 'Operario', 3, 'pqrs@gmail.com', 122131321, 12, 'ACTIVO');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `empresa`
--
ALTER TABLE `empresa`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `gerente_empresa`
--
ALTER TABLE `gerente_empresa`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_usuario` (`id_usuario`),
  ADD KEY `id_empresa` (`id_empresa`);

--
-- Indices de la tabla `pqrs`
--
ALTER TABLE `pqrs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_crea` (`user_crea`),
  ADD KEY `id_empresa` (`id_empresa`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `rol` (`rol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `empresa`
--
ALTER TABLE `empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `gerente_empresa`
--
ALTER TABLE `gerente_empresa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `pqrs`
--
ALTER TABLE `pqrs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `gerente_empresa`
--
ALTER TABLE `gerente_empresa`
  ADD CONSTRAINT `gerente_empresa_ibfk_1` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `gerente_empresa_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `usuario` (`id`) ON UPDATE CASCADE;

--
-- Filtros para la tabla `pqrs`
--
ALTER TABLE `pqrs`
  ADD CONSTRAINT `pqrs_ibfk_1` FOREIGN KEY (`user_crea`) REFERENCES `usuario` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `pqrs_ibfk_2` FOREIGN KEY (`id_empresa`) REFERENCES `empresa` (`id`);

--
-- Filtros para la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD CONSTRAINT `usuario_ibfk_1` FOREIGN KEY (`rol`) REFERENCES `rol` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
