<!DOCTYPE html>
<html lang="en">
<?php

session_start();
if (!isset($_SESSION['id_usuario']) or !isset($_SESSION['id_usuario'])) {
    header('Location: http://localhost/proyect_pqrs/');
}
?>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Inicio</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
    <link href="../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <link href="../dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <link href="../dist/plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
    <link href="../dist/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="CSS/stilos.css">
    <script src="../core/angular/angular.min.js" type="text/javascript"></script>
    <script src="../core/angular/angular-animate.min.js" type="text/javascript"></script>
    <script src="../core/angular/angular-cookies.min.js" type="text/javascript"></script>
    <script src="../core/angular/angular-loader.min.js" type="text/javascript"></script>
    <script src="../core/angular/angular-message-format.min.js" type="text/javascript"></script>
    <script src="../core/angular/angular-messages.min.js" type="text/javascript"></script>
    <script src="../core/angular/angular-mocks.js" type="text/javascript"></script>
    <script src="../core/angular/angular-resource.min.js" type="text/javascript"></script>
    <script src="../core/angular/angular-route.min.js" type="text/javascript"></script>
    <script src="../core/angular/angular-sanitize.min.js" type="text/javascript"></script>
    <script src="../core/angular/angular-touch.min.js" type="text/javascript"></script>
    <script src="../core/node_modules/angular-input-masks/releases/angular-input-masks.js"></script>
    <script src="../core/node_modules/angular-input-masks/releases/angular-input-masks-standalone.js"></script>
    <script src="../core/node_modules/angular-input-masks/releases/angular-input-masks-dependencies.min.js"></script>
    <script src="../core/ui-select/select.min.js"></script>
    <script src="../core/ocLazyLoad/ocLazyLoad.js"></script>
    <script src="../core/angular-ui-router/angular-ui-router.js"></script>
    <script type="text/javascript" src="../core/bootstrap/ui-bootstrap-tpls-0.12.0.js"></script>
    <script type="text/javascript" src="../core/sweet-alert/sweetalert2.all.min.js"></script>
    <script type="text/javascript" src="../core/sweet-alert/sweet-alert.js"></script>
    <link href="../core/ui-select/select.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../core/selectize/selectize.default.css">
    <link rel="stylesheet" href="../CSS/stilos.css">
    <!-- jQuery -->
    <script src="plugins/jquery/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- AdminLTE -->
    <script src="../dist/js/adminlte.js"></script>
    <script src="../dist/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- OPTIONAL SCRIPTS -->
    <script src="plugins/chart.js/Chart.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="../JS/general_controller.js"></script>
    <script src="../JS/controller_home.js"></script>
</head>

<body ng-app="myApp" ng-controller="controller_home">
    <ul>
        <li class="grab" ng-click="objeto.vista = 'INICIO'"><a>Inicio</a></li>
        <li class="grab" ng-if="objeto.rol == 1" ng-click="objeto.vista = 'CREAR_PQRS';ListarEmpresas();"><a>Crear PQRS</a></li>
        <li class="grab" ng-if="objeto.rol == 1" ng-click="objeto.vista = 'LISTA_PQRS';ListarPqrs();"><a>Lista PQRS</a></li>
        <li class="grab" ng-if="objeto.rol == 2" ng-click="objeto.vista = 'CREAR_OPERARIOS'"><a>Crear operarios</a></li>
        <li class="grab" ng-if="objeto.rol == 2" ng-click="objeto.vista = 'LISTA_OPERARIOS';ListarOperarios();"><a>Lista operarios</a></li>
        <li class="grab" ng-if="objeto.rol == 2" ng-click="objeto.vista = 'LISTA_ESTADISTICA';BuscarEstadistica();"><a>Estadística</a></li>
        <li class="grab" ng-if="objeto.rol == 3" ng-click="objeto.vista = 'LISTA_RESPUESTA_PQRS';ListarpqrsPendientes();"><a>PQRS pendientes </a></li>
        <li class="grab" ng-if="objeto.rol == 4" ng-click="objeto.vista = 'CREAR_USUARIOS_GERENTES';ListarEmpresas();"><a>Crear gerentes</a></li>
        <li class="grab" ng-if="objeto.rol == 4" ng-click="objeto.vista = 'LISTA_USUARIOS_GERENTES';ListarGerentes();"><a>Lista gerentes</a></li>
        <li class="grab" ng-if="objeto.rol == 4" ng-click="objeto.vista = 'HISTORICO_PQRS';ListarHistoricoPqrs();"><a>Histórico PQRS</a></li>
        <li class="grab" ng-if="objeto.rol == 4" ng-click="objeto.vista = 'TOTAL_EMPRESAS';ListarTotalEmpresas();objeto.PagesnewEmpresa = 'NO'"><a>Empresas</a></li>
        <li class="grab" style="float:right">
            <form action="../php/modelo/cerrar_sesion.php" method="POST">
                <button type="submit" class="active">Cerrar sesión </button>
                <!-- <a class="active" href="#about">Cerrar sesión</a> -->
            </form>
        </li>

    </ul>
    <div>
        <!-- <a ui-sref="pqrs">PQRS</a> -->
        <section class="content">
            <!-- Inicio formulario para crear la pqrs -->
            <div class="container" ng-if="objeto.vista == 'CREAR_PQRS'">
                <form name="NewPqrs">
                    <p style="font-size: 12px;">Todos los campos son obligatorios</p>
                    <div class="row">
                        <div class="col-25">
                            <label for="fname">Nombre completo</label>
                        </div>
                        <div class="col-75">
                            <input type="text" ng-required="true" ng-disabled="true" ng-model="objeto.nombre_completo" placeholder="Nombre completo">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="lname">Correo electrónico</label>
                        </div>
                        <div class="col-75">
                            <input type="mail" ng-required="true" ng-model="objeto.email" placeholder="Correo electrónico para recibir respuesta">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="lname">Título de petición</label>
                        </div>
                        <div class="col-75">
                            <input type="text" ng-required="true" ng-model="objeto.titulo_pqrs" placeholder="Ingrese el titulo">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="country">Tipo de PQRS</label>
                        </div>
                        <div class="col-75">
                            <select ng-required="true" ng-model="objeto.tipo_pqrs">
                                <option value="PETICION">PETICION</option>
                                <option value="QUEJA">QUEJA</option>
                                <option value="RECLAMO">RECLAMO</option>
                                <option value="SUGERENCIA">SUGERENCIA</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="country">Empresa</label>
                        </div>
                        <div class="col-75">
                            <select ng-required="true" ng-model="objeto.id_empresa">
                                <option ng-repeat="item in temporalEmpresas" value="{{item.id_empresa}}">{{item.nombre_empresa}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="subject">Descripción </label>
                        </div>
                        <div class="col-75">
                            <textarea ng-required="true" ng-model="objeto.descrip_pqrs" placeholder="Ingrese la descripción de la PQRS" style="height:200px"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <p ng-if="NewPqrs.$invalid">Falta responder preguntas</p>
                        <input type="submit" value="Guardar" ng-disabled="NewPqrs.$invalid" ng-click="CrearPqrs();">
                    </div>
                </form>
            </div>
            <!-- Fin formulario para crear la pqrs -->
            <!-- Inicio para ver todas las pqrs creadas por el usuario logueado -->
            <div ng-if="objeto.vista == 'LISTA_PQRS'">
                <div style="overflow-x:auto;">
                    <table id="customers">
                        <tr>
                            <th>Estado</th>
                            <th>Fecha creación</th>
                            <th>Usuario</th>
                            <th>Tipo</th>
                            <th>Correo</th>
                            <th>Titulo</th>
                            <th>Descripción </th>
                        </tr>
                        <tr ng-repeat="item in temporalpqrs">
                            <td>{{item.estado}}</td>
                            <td>{{item.fecha_creacion}}</td>
                            <td>{{item.nombre_completo}}</td>
                            <td>{{item.tipo}}</td>
                            <td>{{item.email}}</td>
                            <td>{{item.titulo}}</td>
                            <td>{{item.descri}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- Fin para ver todas las pqrs creadas por el usuario logueado -->
            <!-- Inicio de la vista para crear operarios -->
            <div class="container" ng-if="objeto.vista == 'CREAR_OPERARIOS'">
                <p style="font-size: 12px;">Todos los campos son obligatorios</p>
                <form name="NewOperario">
                    <div class="row">
                        <div class="col-25">
                            <label for="fname">Nombres:</label>
                        </div>
                        <div class="col-75">
                            <input ng-required="true" ng-model="objeto.newnombre_user" class="form-control" maxlength="100" type="text" placeholder="Nombre">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="lname">Apellidos</label>
                        </div>
                        <div class="col-75">
                            <input ng-required="true" ng-model="objeto.newapellido_user" class="form-control" maxlength="100" type="text" placeholder="Apellidos">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="lname">Numero de documento</label>
                        </div>
                        <div class="col-75">
                            <input ng-required="true" ng-model="objeto.newadocumento_user" class="form-control" type="number" placeholder="Numero de documento">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="lname">Correo electrónico</label>
                        </div>
                        <div class="col-75">
                            <input ng-required="true" ng-model="objeto.newemail_user" class="form-control" type="email" placeholder="Correo electrónico">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="lname">Nombre de usuario</label>
                        </div>
                        <div class="col-75">
                            <input ng-required="true" ng-model="objeto.newname_user" class="form-control" maxlength="50" type="text" placeholder="Nombre de usuario">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="lname">Contraseña</label>
                        </div>
                        <div class="col-75">
                            <input ng-required="true" ng-model="objeto.newpassword" class="form-control" maxlength="50" type="text" placeholder="Ingrese contraseña">
                        </div>
                    </div>
                    <div class="row">
                        <p ng-if="NewOperario.$invalid" style="color: red;">Falta responder preguntas</p>
                        <input type="submit" value="Guardar" ng-disabled="NewOperario.$invalid" ng-click="CrearOperario();">
                    </div>
                </form>
            </div>
            <!-- Fin de la vista para crear los operarios -->
            <!-- Inicio vista para traer los operarios creados por el gerente -->
            <div ng-if="objeto.vista == 'LISTA_OPERARIOS'">
                <div style="overflow-x:auto;">
                    <table id="customers">
                        <tr>
                            <th>Acción</th>
                            <th>Estado</th>
                            <th>Usuario</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Correo</th>
                        </tr>
                        <tr ng-repeat="item in temporalOperarios">
                            <th ng-click="EditarOperario(item);">Editar</th>
                            <td>
                                <select ng-model="item.oper_estado">
                                    <option value="ACTIVO">ACTIVO</option>
                                    <option value="INACTIVO">INACTIVO</option>
                                </select>
                            </td>
                            <td>{{item.oper_user}}</td>
                            <td><input type="text" ng-model="item.oper_nombres"></td>
                            <td><input type="text" ng-model="item.oper_apellidos"></td>
                            <td><input type="mail" ng-model="item.oper_email"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- Fin vista de operarios creados por el gerente -->
            <!-- Inicio de total de pqrs pendientes -->
            <div ng-if="objeto.vista == 'LISTA_RESPUESTA_PQRS'">
                <div style="overflow-x:auto;">
                    <table id="customers">
                        <tr>
                            <th>Acción</th>
                            <th>Estado</th>
                            <th>Empresa</th>
                            <th>Fecha creación</th>
                            <th>Usuario</th>
                            <th>Tipo</th>
                            <th>Correo</th>
                            <th>Titulo</th>
                            <th>Descripción </th>
                            <th>Respuesta </th>
                        </tr>
                        <tr ng-repeat="item in temporalPqrsPendientes">
                            <td>
                                <a class="grab" ng-click="ResponderPqrs(item);" ng-if="item.diferencia < 1">Responder</a>
                                <a style="font-size: 10px;color:red;" ng-if="item.diferencia >= 1">Tiempo de respuesta excedido</a>
                            </td>
                            <td>{{item.estado}}</td>
                            <td>{{item.name_empresa}}</td>
                            <td>{{item.fecha_creacion}}</td>
                            <td>{{item.nombre_completo}}</td>
                            <td>{{item.tipo}}</td>
                            <td>{{item.email}}</td>
                            <td>{{item.titulo}}</td>
                            <td>{{item.descri}}</td>
                            <td><textarea ng-model="item.respuesta" placeholder="Ingrese la respuesta de la PQRS" style="height:50px"></textarea></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- Fin de vista para las respuestas de PQRS pendientes -->
            <!-- Inicio de vista para crear los usuarios tipo gerentes -->
            <div class="container" ng-if="objeto.vista == 'CREAR_USUARIOS_GERENTES'">
                <p style="font-size: 12px;">Todos los campos son obligatorios</p>
                <form name="NewGerente">
                    <div class="row">
                        <div class="col-25">
                            <label for="fname">Nombres:</label>
                        </div>
                        <div class="col-75">
                            <input ng-required="true" ng-model="objeto.Gerenombre_user" class="form-control" maxlength="100" type="text" placeholder="Nombre">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="lname">Apellidos</label>
                        </div>
                        <div class="col-75">
                            <input ng-required="true" ng-model="objeto.Gereapellido_user" class="form-control" maxlength="100" type="text" placeholder="Apellidos">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="lname">Numero de documento</label>
                        </div>
                        <div class="col-75">
                            <input ng-required="true" ng-model="objeto.Gereadocumento_user" class="form-control" type="number" placeholder="Numero de documento">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="lname">Correo electrónico</label>
                        </div>
                        <div class="col-75">
                            <input ng-required="true" ng-model="objeto.Gereemail_user" class="form-control" type="email" placeholder="Correo electrónico">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="lname">Nombre de usuario</label>
                        </div>
                        <div class="col-75">
                            <input ng-required="true" ng-model="objeto.Gerename_user" class="form-control" maxlength="50" type="text" placeholder="Nombre de usuario">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="country">Empresa</label>
                        </div>
                        <div class="col-75">
                            <select ng-blur="ValidarAsociacionEmpresa();" ng-required="true" ng-model="objeto.Gereid_empresa">
                                <option ng-repeat="item in temporalEmpresas" value="{{item.id_empresa}}">{{item.nombre_empresa}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-25">
                            <label for="lname">Contraseña</label>
                        </div>
                        <div class="col-75">
                            <input ng-required="true" ng-model="objeto.Gerepassword" class="form-control" maxlength="50" type="text" placeholder="Ingrese contraseña">
                        </div>
                    </div>
                    <div class="row">
                        <p ng-if="NewGerente.$invalid" style="color: red;">Falta responder preguntas</p>
                        <input type="submit" value="Guardar" ng-disabled="NewGerente.$invalid" ng-click="GuardarGerentes();">
                    </div>
                </form>
            </div>
            <!-- Fin de vista para crear los usuarios tipo gerentes -->
            <!-- Inicio vista usuarios tipo gerentes -->
            <div ng-if="objeto.vista == 'LISTA_USUARIOS_GERENTES'">
                <div style="overflow-x:auto;">
                    <table id="customers">
                        <tr>
                            <th>Acción</th>
                            <th>Estado</th>
                            <th>Empresa</th>
                            <th>Usuario</th>
                            <th>Nombres</th>
                            <th>Apellidos</th>
                            <th>Correo</th>
                        </tr>
                        <tr ng-repeat="item in temporalGerentes">
                            <th class="grab" ng-click="EditarGerente(item);">Editar</th>
                            <td>
                                <select ng-model="item.user_estado">
                                    <option value="ACTIVO">ACTIVO</option>
                                    <option value="INACTIVO">INACTIVO</option>
                                </select>
                            </td>
                            <td>
                                {{item.id_nombre_empresa}}
                            </td>
                            <td>{{item.user_u}}</td>
                            <td><input type="text" ng-model="item.nombres_user"></td>
                            <td><input type="text" ng-model="item.apellidos_user"></td>
                            <td><input type="mail" ng-model="item.user_email"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- Fin de vista usuarios tipo gerentes -->
            <!-- Inicio vista historico PQRS -->
            <div ng-if="objeto.vista == 'HISTORICO_PQRS'">
                <div style="overflow-x:auto;">
                    <table id="customers">
                        <tr>
                            <th>Estado</th>
                            <th>Empresa</th>
                            <th>Tiempo</th>
                            <th>Fecha creación</th>
                            <th>Fecha realizada </th>
                            <th>Usuario</th>
                            <th>Tipo</th>
                            <th>Correo</th>
                            <th>Titulo</th>
                            <th>Descripción </th>
                            <th>Respuesta</th>
                        </tr>
                        <tr ng-repeat="item in temporalHistorico">
                            <td>{{item.estado}}</td>
                            <td>{{item.name_empresa}}</td>
                            <td>
                                <a ng-if="item.diferencia != '-00'">Tiempo excedido</a>
                                <a ng-if="item.diferencia == '-00'">Respuesta a tiempo</a>
                            </td>
                            <td>{{item.fecha_creacion}}</td>
                            <td>
                                <a style="color:black;" ng-if="item.fecha_respuesta_modificada =='0000-00-00 00:00:00'">Pendiente</a>
                                <a style="color:black;" ng-if="item.fecha_respuesta_modificada !='0000-00-00 00:00:00'">{{item.fecha_respuesta_modificada}}</a>
                            </td>
                            <td>{{item.nombre_completo}}</td>
                            <td>{{item.tipo}}</td>
                            <td>{{item.email}}</td>
                            <td>{{item.titulo}}</td>
                            <td>{{item.descri}}</td>
                            <td>{{item.respuesta}}</td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- Fin historico pqrs -->
            <!-- Inicio vista de CRUD empresas -->
            <div ng-if="objeto.vista == 'TOTAL_EMPRESAS'">
                <div ng-if="objeto.PagesnewEmpresa == 'SI'">
                    <p style="font-size: 12px;">Todos los campos son obligatorios</p>
                    <form name="NewEmpresa">
                        <div class="row">
                            <div class="col-25">
                                <label for="fname">Nombre:</label>
                            </div>
                            <div class="col-75">
                                <input ng-required="true" ng-model="objeto.NewEmpresa" class="form-control" maxlength="100" type="text" placeholder="Nombre de la empresa  ">
                            </div>
                        </div>
                        <div class="row">
                            <p ng-if="NewEmpresa.$invalid" style="color: red;">Falta responder preguntas</p>
                            <input type="submit" value="Guardar" ng-disabled="NewEmpresa.$invalid" ng-click="GuardarNewEmpresa();">
                        </div>
                    </form>
                </div>
                <div style="overflow-x:auto;">
                    <table id="customers">
                        <tr>
                            <th ng-if="objeto.PagesnewEmpresa == 'NO'" class="grab" style="width: 150px;" ng-click="objeto.PagesnewEmpresa = 'SI'">Crear</th>
                            <th ng-if="objeto.PagesnewEmpresa == 'SI'" class="grab" style="width: 150px;background-color: gray;" ng-click="objeto.PagesnewEmpresa = 'NO'">Cancelar </th>
                            <th>Estado</th>
                            <th>Nombre de empresa</th>
                        </tr>
                        <tr ng-repeat="item in temporalEmpresas">
                            <td>
                                <!-- <input type="reset" value="Eliminar " ng-click="EliminarEmpresa(item);"> -->
                                <input ng-click="EditarEmpresa(item);" type="button" class="botones" value="Editar">
                            </td>
                            <td>
                                <select ng-model="item.estado_empresa">
                                    <option value="ACTIVO">ACTIVO</option>
                                    <option value="INACTIVO">INACTIVO</option>
                                </select>
                            </td>
                            <td><input type="text" ng-model="item.nombre_empresa"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <!-- Fin vista empresas -->
            <!-- Inicio de vista de las estadisticas de pqrs para gerente-->
            <div class="container" ng-if="objeto.vista == 'LISTA_ESTADISTICA'">
                <div id="piechart"></div>
            </div>
            <!-- Fin de vista de las estadisticas de las pqrs -->
        </section>
    </div>
</body>
<!-- Controlador js -->

</html>